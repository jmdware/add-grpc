package net1493.addgrpc.server;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.grpc.Server;
import io.grpc.ServerBuilder;

public class AddServer {

    private final Logger logger = LoggerFactory.getLogger(AddServer.class);

    private final int port;

    private final Server server;

    public AddServer(int port) {
        this.port = port;
        server = ServerBuilder.forPort(port).addService(new AddService()).build();
    }

    public void start() throws IOException {
        server.start();
        logger.info("Server started, listening on {}", port);
        Runtime.getRuntime().addShutdownHook(new Thread(AddServer.this::stop));
    }

    public void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    public static void main(String[] args) throws Exception {
        AddServer server = new AddServer(50051);
        server.start();
        server.blockUntilShutdown();
    }
}
