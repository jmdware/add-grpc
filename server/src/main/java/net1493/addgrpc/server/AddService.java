package net1493.addgrpc.server;

import io.grpc.stub.StreamObserver;
import net1493.addgrpc.AddRequest;
import net1493.addgrpc.AddResponse;
import net1493.addgrpc.AddServiceGrpc;

public class AddService extends AddServiceGrpc.AddServiceImplBase {
    @Override
    public void add(AddRequest req, StreamObserver<AddResponse> responseObserver) {
        AddResponse response = AddResponse.newBuilder()
                .setSum(req.getNum1() + req.getNum2())
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
