package net1493.addgrpc.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.codahale.metrics.JmxReporter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.Timer.Context;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import net1493.addgrpc.AddRequest;
import net1493.addgrpc.AddResponse;
import net1493.addgrpc.AddServiceGrpc;
import net1493.addgrpc.AddServiceGrpc.AddServiceBlockingStub;

public class AddClient {
    public static void main(String[] args) throws IOException {
        int numThreads;

        if (args.length != 1) {
            numThreads = 1;
        } else {
            numThreads = Integer.parseInt(args[0]);
        }

        MetricRegistry metricRegistry = new MetricRegistry();

        JmxReporter jmxReporter = JmxReporter.forRegistry(metricRegistry)
                .inDomain("add-client")
                .convertDurationsTo(TimeUnit.MICROSECONDS)
                .build();
        jmxReporter.start();

        Timer timer = metricRegistry.timer("add");

        AddRequest addRequest = AddRequest.newBuilder().setNum1(11).setNum2(17).build();

        ManagedChannel channel = ManagedChannelBuilder.forAddress("127.0.0.1", 50051)
                .usePlaintext(true)
                .build();

        ExecutorService executor = Executors.newFixedThreadPool(numThreads);

        try {
            AddServiceBlockingStub stub = AddServiceGrpc.newBlockingStub(channel);

            for (int i = 0; i < numThreads; i++) {
                executor.submit(() -> {
                    int count = 0;

                    while (!Thread.currentThread().isInterrupted()) {
                        try (Context ignored = timer.time()) {
                            AddResponse response = stub.add(addRequest);

                            if (count == 10_000) {
                                count = 0;
                                System.out.println(response);
                            } else {
                                count++;
                            }
                        }
                    }
                });
            }

            System.out.println("Press any key to stop . . .");

            new BufferedReader(new InputStreamReader(System.in)).read();
        } finally {
            channel.shutdownNow();
            executor.shutdownNow();
            jmxReporter.stop();
        }
    }
}
